function(jtsTracker, oGateConfig){
//Test
	this.oConf = oGateConfig;
	this.oTracker = jtsTracker;
	this.youTubeVideoList = [];

	this.init = function(){
		// we load the data only. 
		// when YT API is loaded, it executes window.onYouTubeIframeAPIReadyby itself.
		this.loadContent();
	}
	
	this.loadContent = function () {
		var loadingIntervalCounter = 5;

		var loadingInterval = setInterval(function () {
			this.youTubeVideoList = document.querySelectorAll('iframe[src*="youtube.com"]');

			// we only load YouTube API if videos are found
			// and the API isn't already loaded
			if (this.youTubeVideoList.length) {
				
				// if enforcing jsapi is enabled, we add the parameter and reload the iframes
				if (this.oConf.apisupport) {
					for (var i = 0; i < this.youTubeVideoList.length; i++) {
						var url = new URL(this.youTubeVideoList[i].src);
						var jsapi = url.searchParams.get("enablejsapi");

						if (jsapi === null || jsapi !== "1") {
							url.searchParams.set("enablejsapi", 1);
							this.youTubeVideoList[i].src = url.toString();
						}
					}
				}

				// if the YT API isn't loaded yet, we load it ourselves
				if (typeof(YT) === 'undefined') {
					var tag = document.createElement('script');
					tag.id = 'youtube-iframe-api';
					tag.src = 'https://www.youtube.com/iframe_api';
					var firstScriptTag = document.getElementsByTagName('script')[0];
					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
				}

			}

			// if the videos are loaded or interval has run out
			// this is to handle slower loading sites
			if (loadingIntervalCounter <= 0 || this.youTubeVideoList.length) {
				clearInterval(loadingInterval);
			}

			loadingIntervalCounter--;
		}.bind(this), 500);
	};

	this.getStatusInStringFormat = function() {
		var status = {};

		status[YT.PlayerState.PLAYING] = "play";
		status[YT.PlayerState.BUFFERING] = "seek";
		status[YT.PlayerState.PAUSED] = "pause";
		status[YT.PlayerState.ENDED] = "compvare";

		return status;
	};

	this.addEventsToPlayers = function() {
			var intervalHandlers = []; // references to the intervals set
			var valuesSent = []; // which % values were sent already for certain video
			var progressValues = this.oConf.progress.split("|");
			var videoStarted = []; // timestamps when user clicked play for the first time
			var statusToString = this.getStatusInStringFormat(); // convert integer status codes to string

			const pushToDataLayer = function(player, index) {
				window._jts.push({
					"track": "video",
					"provider": "youtube",
					"start": videoStarted[index],
					"duration": player.getDuration(),
					"currenttime": player.getCurrentTime(),
					"percent": calculatePercentage(player),
					"status": statusToString[player.getPlayerState()],
					"url": player.getVideoUrl(),
					"title": player.getVideoData().title,
					"id": player.getVideoData().video_id
				});
			};

			const calculatePercentage = function(player) {
				return Math.round(player.getCurrentTime() / player.getDuration() * 100);
			};

			const eventFilter = function(player, index) {
				var send = false;
				var playerState = player.getPlayerState();

				// we are not tracking CUED and UNSTARTED in any case
				if (playerState === YT.PlayerState.CUED || playerState === YT.PlayerState.UNSTARTED) {
					return false;
				}

				// if video is started for the first time, we must log the time and broadcast it (if it is set in the config)
				if (playerState === YT.PlayerState.PLAYING) {
					if (this.oConf.start) {
						if (!videoStarted[index]) {
							send = true;
							videoStarted[index] = new Date().getTime();
						}
					}
				} 
				
				// we track these events only if user is interacting with the video(not before the actual start)
				if (videoStarted[index]) {
					if (
						playerState === YT.PlayerState.ENDED &&
						this.oConf.compvare
					) {
						send = true;
					} else if (this.oConf.psb) {
						var status = [
							YT.PlayerState.BUFFERING, YT.PlayerState.PAUSED
						];

						if (status.includes(playerState)) {
							send = true;
						}
					}
				}

				if (send) {
					pushToDataLayer(player, index);
				}
			}.bind(this);

			const interval = function(player, index) {
				const currentPercentage = calculatePercentage(player);
				
				for (var i = 0; i < progressValues.length; i++) {
					var limitPercentage = progressValues[i];

					// if we have already sent the event for certain %, we don't do it again
					if (currentPercentage >= limitPercentage && !valuesSent[index].includes(limitPercentage)) {
						pushToDataLayer(player, index);
						valuesSent[index].push(limitPercentage);
					}
				}

			};

			const playerStateChange = function(index) {
				return function (event) {
					var player = event.target;

					// we send the state change to eventFilter functions which decides
					// whether to push it to the datalayer or not
					eventFilter(player, index);

					// setting the interval, so we can track the % in real time
					if (player.getPlayerState() === YT.PlayerState.PLAYING) {
						intervalHandlers[index] = setInterval(interval, 1000, player, index);
					} else {
						clearInterval(intervalHandlers[index]);
					}
				};
			};

			for (var i = 0; i < this.youTubeVideoList.length; i++) {
				valuesSent[i] = [];
				videoStarted[i] = false;

				var player = new YT.Player(this.youTubeVideoList[i], {
						events: {
							'onStateChange': playerStateChange(i)
						}
				});
			}

	}

	window.onYouTubeIframeAPIReady = function() {
		
		this.addEventsToPlayers();

	}.bind(this);

	this.init();
}